# Description

A hypothetical inventory manager written in the Go programming
language.  This is based off of a coding exercise given to potential
interviewees.

# Usage

    -input-file="": Name of input file
    -use-random=false: Generate orders randomly
    -use-test-data=false: Use testing data

