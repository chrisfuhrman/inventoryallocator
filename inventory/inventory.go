// ====================================================================
//
// inventory.go
//
// Copyright (c) 2015 Shipwire, Inc.
// All rights reserved
//
// Created Thu Mar 26 08:13:10 2015 PDT
//
// ====================================================================

package inventory

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	order "github.com/cfuhrman/inventoryAllocator/order"
)

type ProductStock struct {
	Store map[string]int
}

var (
	logger *log.Logger
	orderCounter int
)

// Initializes Inventory Store with test data
func InitializeTest() *ProductStock {
	inventoryStore := ProductStock {
		Store: make(map[string]int),
	}

	store := map[string]int {
		"A" : 2,
		"B" : 3,
		"C" : 1,
		"D" : 0,
		"E" : 0,
	}

	inventoryStore.Store = store

	return &inventoryStore
}

// Initializes Inventory Store as prod data
func InitializeProd() *ProductStock  {
	inventoryStore := ProductStock {
		Store: make(map[string]int),
	}

	store := map[string]int {
		"A" : 150,
		"B" : 150,
		"C" : 150,
		"D" : 150,
		"E" : 150,
	}

	inventoryStore.Store = store

	return &inventoryStore
}

// Returns the number of products in the inventory store
func (inventory *ProductStock) Count() int  {
	return len(inventory.Store);
}

// Returns an array listing unique products
func (inventory *ProductStock) GetUniqueProducts() []string  {
	var productList []string

	for product, _ := range inventory.Store {
		productList = append(productList, product)
	}

	return productList
}

// Generates a random Order
func (inventory *ProductStock) GenerateRandomOrder(randomError bool) order.Order  {
	var orderLines []order.OrderLine

	productsPresent := make(map[string]bool)
	productsList	:= inventory.GetUniqueProducts()
	productCount	:= rand.Intn(inventory.Count()) + 1

	orderCounter++

	for i := 0; i < productCount; i++ {
		for  {
			product := productsList[rand.Intn(len(productsList))]

			if productsPresent[product] {
				continue
			} else {
				quantity := rand.Intn(order.ORDER_MAXIMUM) + 1
				line := order.OrderLine {
					Product		: product,
					Quantity	: strconv.Itoa(quantity),
				}

				// Do we generate a random invalid quantity?
				if randomError && rand.Intn(20) == 0 {
					randomQuantity := order.ORDER_MAXIMUM + rand.Intn(3)
					line.Quantity = strconv.Itoa(randomQuantity)
				}

				orderLines			= append(orderLines, line)
				productsPresent[product]	= true
				break
			}
		}
	}

	order := order.Order {
		Header	: orderCounter,
		Lines	: orderLines,
	}

	return order

}

// Determines if all product stock is empty
func (inventory *ProductStock) IsEmpty() bool  {
	for _, quantity := range inventory.Store {
		if quantity > 0 {
			return false
		}
	}

	return true
}

// Pretty Prints Inventory
func (inventory *ProductStock) PrettyPrint () {
	var output bytes.Buffer

	b, _ := json.Marshal(inventory)

	json.Indent(&output, b, "", "\t")

	output.WriteTo(os.Stdout)
	fmt.Print("\n")
}

// Processes an order, returning statistics as to how each product in
// the order was allocated.
func (inventory *ProductStock) ProcessOrder(o order.Order) order.OrderStatistics {
	stats := order.OrderStatistics {
		OrderID: o.Header,
		Disposition: make(map[string]*order.OrderDisposition),
	}

	if !o.IsValid() {
		logger.Printf("Order is not valid: ", o.Header)
		return stats
	}

	// Initialize statistics for this order
	for product, _ := range inventory.Store {
		disposition := order.OrderDisposition {
			Requested: 0,
			Allocated: 0,
			BackOrdered: 0,
		}

		stats.Disposition[product]	= &disposition
	}

	// Attempt to process the order lines
	for _, request := range o.Lines {
		product		:= request.Product
		quantity, _	:= strconv.Atoi(request.Quantity)
		stats.Disposition[product].Requested = quantity

		if inventory.Store[product] < quantity {
			stats.Disposition[product].BackOrdered	= quantity - inventory.Store[product]
			stats.Disposition[product].Allocated	= inventory.Store[product]
			inventory.Store[product]		= 0
		} else {
			stats.Disposition[product].Allocated	= quantity
			inventory.Store[product]		= inventory.Store[product] - quantity
		}
	}

	return stats

}

// Setter for object logger
func SetLogger(l *log.Logger)  {
	logger = l
	logger.Println("Logger has been set")
}

// Ende
