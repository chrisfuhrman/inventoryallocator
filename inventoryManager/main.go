// ====================================================================
//
// main.go
//
// Copyright (c) 2015 Shipwire, Inc.
// All rights reserved
//
// Created Thu Mar 26 10:05:19 2015 PDT
//
// ====================================================================

package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	order     "github.com/cfuhrman/inventoryAllocator/order"
	inventory "github.com/cfuhrman/inventoryAllocator/inventory"
)

var (
	logger *log.Logger
)

// Processes Orders from input file
func processFile(inputFilePtr *string, stock *inventory.ProductStock) order.StatisticsCollection  {

	var stats order.StatisticsCollection

	logger.Printf("Reading Input File %s", *inputFilePtr)
	orderList, err := os.Open(*inputFilePtr)

	if err != nil {
		logger.Fatalf("Cannot open input file %s", *inputFilePtr)
	}

	dec := json.NewDecoder(orderList)

	defer orderList.Close()

	for  {
		var o order.Order

		if stock.IsEmpty() {
			logger.Println("Stock is exhausted")
			break
		}

		err := dec.Decode(&o)

		if err != nil {

			if err.Error() == "EOF" {
				break
			} else {
				logger.Printf("Error: Cannot parse: %s", err.Error())
			}

		} else {
			stats.Statistics = append(stats.Statistics, stock.ProcessOrder(o))
		}
	}

	return stats

}

// Processes randomly generated orders
func processRandomStream(stock *inventory.ProductStock) order.StatisticsCollection{

	var stats order.StatisticsCollection

	for  {
		if stock.IsEmpty() {
			logger.Println("Stock is exhausted")
			break
		}

		o := stock.GenerateRandomOrder(false)
		stats.Statistics = append(stats.Statistics, stock.ProcessOrder(o))
	}

	return stats

}

// The main function
func main()  {
	logger = log.New(os.Stderr, "inventoryManager: ", log.Ldate | log.Ltime | log.Lshortfile)

	var stock *inventory.ProductStock

	inputFilePtr := flag.String("input-file", "", "Name of input file")
	useRandomPtr := flag.Bool("use-random", false, "Generate orders randomly")
	useTestPtr   := flag.Bool("use-test-data", false, "Use testing data")

	flag.Parse()
	inventory.SetLogger(logger)

	if *useTestPtr {
		logger.Println("Using test data")
		stock = inventory.InitializeTest()
	} else {
		logger.Println("Using prod data")
		stock = inventory.InitializeProd()
	}

	// stock.PrettyPrint()

	if *inputFilePtr != "" {
		stats := processFile(inputFilePtr, stock)
		stats.PrettyPrint()
	} else if *useRandomPtr {
		stats := processRandomStream(stock)
		stats.PrettyPrint()
	} else {
		logger.Fatalln("Goes Nowhere, Does Nothing")
	}

}

// Ende
