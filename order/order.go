// ====================================================================
//
// order.go
//
// Copyright (c) 2015 Shipwire, Inc.
// All rights reserved
//
// Created Wed Mar 25 16:21:14 2015 PDT
//
// ====================================================================

package order

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)

type OrderLine struct {
	Product  string
	Quantity string
}

type Order struct {
	Header     int
	Lines      []OrderLine
}

type OrderDisposition struct {
	Requested   int
	Allocated   int
	BackOrdered int
}

type OrderStatistics struct {
	OrderID     int
	Disposition map[string]*OrderDisposition
}

type StatisticsCollection struct {
	Statistics []OrderStatistics
}

const ORDER_MINIMUM int = 0
const ORDER_MAXIMUM int = 5

// Initializes an order object from a JSON string
func InitializeFromJSON (jsonString string) Order {
	var orders []Order
	var order Order

	if err := json.Unmarshal([]byte(jsonString), &orders); err != nil {
		order = orders[0]
	} else {
		fmt.Println("Oops, an error occurred");
	}

	return order
}

// Converts order object to JSON string
func (order *Order) PrintJSON() {
	var output bytes.Buffer

	b, _ := json.Marshal(order);

	json.Indent(&output, b, "", "\t")

	output.WriteTo(os.Stdout)
	fmt.Print("\n")
}

// Determines if an order is valid
func (order *Order) IsValid() bool {
	for _, line := range order.Lines {
		quantity, _ := strconv.Atoi(line.Quantity)

		if quantity < ORDER_MINIMUM || quantity > ORDER_MAXIMUM {
			return false
		}
	}

	return true
}

// Pretty Prints Statistics in JSON format
func (stats *StatisticsCollection) PrettyPrint() {
	var output bytes.Buffer

	b, _ := json.Marshal(stats.Statistics)

	json.Indent(&output, b, "", "\t")

	output.WriteTo(os.Stdout)
	fmt.Print("\n")
}

// Ende
